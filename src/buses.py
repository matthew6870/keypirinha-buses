import keypirinha as kp
import keypirinha_util as kpu
import keypirinha_net as kpnet
import json
from time import strftime
from datetime import datetime


# TODO add custom action copy stop name, stop ID, copy route
# TODO add some formatting to routes


class Buses(kp.Plugin):
    ACTION_COPY_ROUTE = "copy_route"
    ACTION_COPY_STOP_ID = "copy_stop_id"
    ACTION_COPY_STOP_NAME = "copy_stop_name"

    ITEMCAT_STOP = kp.ItemCategory.USER_BASE + 1
    ITEMCAT_ROUTE = kp.ItemCategory.USER_BASE + 2

    URL_AUTOCOMPLETE = 'https://www.ubian.sk/navigation/autocomplete?cityID=18024&filter=urban&query={}'
    URL_ROUTE = "https://www.ubian.sk/navigation/connections?&maxChanges=1&departure=1&filter=urban&sorting=first&cityID=18024&from[stopID]={start}&to[stopID]={goal}&date={date}"

    TARGET_SPLIT_CHAR = " "

    CONFIG_SECTION_CUSTOM_STOP = "stop"
    CONFIG_START_STOP_ITEM = "start_stop"
    CONFIG_GOAL_STOP_ITEM = "goal_stop"
    CONFIG_DESC_ITEM = "desc"

    opener = kpnet.build_urllib_opener()
    catalog_items = list()

    def __init__(self):
        super().__init__()
        self._debug = True

        self.opener.addheaders = [("User-agent", "Mozilla/5.0")]
        self.dbg("========================================")

    def on_start(self):
        # register actions
        actions_route = [
            self.create_action(
                name=self.ACTION_COPY_ROUTE,
                label="Copy route",
                short_desc="Copy route to clipboard")
        ]
        self.set_actions(self.ITEMCAT_ROUTE, actions_route)

        action_stops = [
            self.create_action(
                name=self.ACTION_COPY_STOP_ID,
                label="Copy stop ID",
                short_desc="Copy stop ID to clipboard"),
            self.create_action(
                name=self.ACTION_COPY_STOP_NAME,
                label="Copy stop name",
                short_desc="Copy stop name to clipboard")
        ]
        self.set_actions(self.ITEMCAT_STOP, action_stops)

        self._get_catalog_items()
        pass

    def on_catalog(self):
        self.set_catalog(self.catalog_items)

    def on_suggest(self, user_input, items_chain):
        if not items_chain:
            return

        current_item = items_chain[len(items_chain) - 1]
        suggestions = self._get_suggestions(user_input, current_item)
        self.set_suggestions(suggestions, kp.Match.ANY, kp.Sort.NONE)

    def on_execute(self, item, action):
        if item.category() == self.ITEMCAT_STOP:
            if action.name() in self.ACTION_COPY_STOP_ID:
                kpu.set_clipboard(item.target().strip())
            if action.name() in self.ACTION_COPY_STOP_NAME:
                kpu.set_clipboard(item.label())

    def _get_suggestions(self, user_input, current_item):
        ids = current_item.target().strip().split(self.TARGET_SPLIT_CHAR)

        if len(ids) == 2:
            suggestions = self._load_routes(ids[0], ids[1])
        else:
            suggestions = self._load_stops(user_input, current_item.target())

        return suggestions

    def _load_stops(self, user_input, current_target):
        suggestions = list()
        if len(user_input) < 2:
            return suggestions

        url = self.URL_AUTOCOMPLETE.format(user_input)
        data = self._ubian_request(url)
        self.dbg(json.dumps(data, indent=4))

        suggestions = list()
        if (data["status"] != "ok"):
            suggestions.append(
                self.create_error_item(label="Error", short_desc="Try again"))
        else:
            results = data["results"]
            for result in results[:10]:
                if 'stopName' not in result:
                    continue
                self.dbg(result["stopName"])
                id = str(result["id"])
                suggestions.append(
                    self.create_item(
                        category=self.ITEMCAT_STOP,
                        label=result["stopName"],
                        short_desc=id,
                        target=f"{current_target} {id}",
                        args_hint=kp.ItemArgsHint.REQUIRED,
                        hit_hint=kp.ItemHitHint.IGNORE,
                        loop_on_suggest=True))

        return suggestions

    def _load_routes(self, start, goal):
        date = strftime("%Y-%m-%d+%H:%M")
        url = self.URL_ROUTE.format(start=start, goal=goal, date=date)
        data = self._ubian_request(url)
        self.dbg(json.dumps(data, indent=4))

        suggestions = list()
        for connection in data["connections"]:
            route = ""
            for part in connection["parts"]:
                if part["timeTableTrip"] is None:
                    continue
                fromStop = part["fromStop"]["stopName"]
                toStop = part["toStop"]["stopName"]
                time = part["fromDepartureTime"].split(" +")
                timeObject = datetime.strptime(time[0], "%a, %d %b %Y %H:%M:%S")
                line = part["timeTableTrip"]["timeTableLine"]["line"]

                x = timeObject.strftime("%H:%M")
                route += f"  {line}|{x}|{fromStop}|{toStop}"

            self.dbg(route)
            suggestions.append(
                self.create_item(
                    category=self.ITEMCAT_ROUTE,
                    label=route.strip(),
                    short_desc=f"{start} {goal}",
                    target=route,
                    args_hint=kp.ItemArgsHint.FORBIDDEN,
                    hit_hint=kp.ItemHitHint.IGNORE))

        return suggestions

    def _ubian_request(self, url):
        self.dbg(url)
        with self.opener.open(url) as conn:
            response = conn.read()
        return json.loads(response)

    def on_events(self, flags):
        if flags & (kp.Events.APPCONFIG | kp.Events.PACKCONFIG):
            self._get_catalog_items()
            self.on_catalog()

    def _get_catalog_items(self):
        settings = self.load_settings()
        for section in settings.sections():
            if not section.lower().startswith(self.CONFIG_SECTION_CUSTOM_STOP + "/"):
                continue
            section_name = section[len(self.CONFIG_SECTION_CUSTOM_STOP) + 1:].strip()
            start = settings.get_stripped(self.CONFIG_START_STOP_ITEM, section)
            goal = settings.get_stripped(self.CONFIG_GOAL_STOP_ITEM, section)
            desc = settings.get_stripped(self.CONFIG_DESC_ITEM, section, fallback="Your custom predefined route")
            if not start or not goal:
                self.warn(f"Catalog item {section_name} was not created. Missing start or goal stop")
                continue

            self.catalog_items.append(
                self.create_item(
                    category=kp.ItemCategory.KEYWORD,
                    label=section_name,
                    short_desc=desc,
                    target=f"{start} {goal}",
                    args_hint=kp.ItemArgsHint.REQUIRED,
                    hit_hint=kp.ItemHitHint.IGNORE,
                    loop_on_suggest=True)
            )

        self.catalog_items.append(
            self.create_item(
                category=kp.ItemCategory.KEYWORD,
                label="Buses",
                short_desc="by Matthew",
                target=self.TARGET_SPLIT_CHAR,
                args_hint=kp.ItemArgsHint.REQUIRED,
                hit_hint=kp.ItemHitHint.IGNORE)
        )
